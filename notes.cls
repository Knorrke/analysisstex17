%%
%% Document Class "Notes"
%% -----------------------------
%%
%%

%%
%% Intro
%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{notes}[2015/03/22]
\typeout{}
\typeout{*** notes.cls, version 1.4 ***}
\typeout{}
\makeatletter
\RequirePackage{ifthen}
\newboolean{@hyper}
\setboolean{@hyper}{false}
\DeclareOption{hyper}{\setboolean{@hyper}{true}}
\makeatother
\DeclareOption*{\typeout{undefined option: \CurrentOption}}
\ProcessOptions\relax
\LoadClass[11pt]{article}

%%
%% Packages
%%

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{pgf}
\RequirePackage{tikz}
\usetikzlibrary{arrows,matrix,decorations.pathmorphing}
\RequirePackage{csquotes}
\RequirePackage{graphicx}
\RequirePackage{xstring}
\RequirePackage{fullpage}
\RequirePackage{sectsty}
\RequirePackage{fancyhdr}
\RequirePackage{tocloft}
\RequirePackage[german]{babel}
\RequirePackage{dsfont}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{fourier}
\RequirePackage{color}
\RequirePackage{framed}
\RequirePackage{titlesec}
\RequirePackage{changepage}
\RequirePackage{xargs}
\RequirePackage{alphalph}

\makeatletter
\ifthenelse{\boolean{@hyper}}{\RequirePackage{hyperref}}
\makeatother

%%
%% Page Layout 
%%

%% Layout Parameter

\setlength{\parindent}{0.0cm}
\setlength{\textwidth}{17cm}
\setlength{\textheight}{22cm}
\setlength{\evensidemargin}{2.25cm}
\setlength{\oddsidemargin}{2.25cm}
\setlength{\headheight}{2.0cm}
\setlength{\headsep}{0cm}
\setlength{\hoffset}{-2.54cm}
\setlength{\voffset}{-2.54cm}
\setlength{\footskip}{1.0cm}
\renewcommand{\smallskip}{\vspace*{0.15cm}}
\renewcommand{\medskip}{\vspace*{0.3cm}}
\renewcommand{\bigskip}{\vspace*{0.7cm}}
\clubpenalty=10000
\widowpenalty=10000

%% Page Marking 

\fancyhead{}
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\vspace*{0.7cm} \------~~~~\thepage~~~~\------}\rfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.6pt}
\fancypagestyle{plain}{%
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.6pt}
\cfoot{\vspace*{0.7cm} \------~~~~\thepage~~~~\------}}


%%
%% Title Page and Table of Contents 
%%

\makeatletter
\renewcommand{\title}[3]{\gdef\@title{#3}\gdef\@titlesize{#1}\gdef\@titlesep{#2}}
\renewcommand{\maketitle}{%
\vspace*{0cm}\begin{center}

\fontsize{\@titlesize}{\@titlesep}\selectfont
{\bfseries \@title} \\[0.5cm]
\fontsize{8}{16}\selectfont
Mitschrift\\[1.5cm]

\fontsize{12}{16}\selectfont 
(Version vom \today)
\end{center}\vspace{1cm}

\begin{center}
\begin{adjustwidth}{2cm}{2cm}
\tableofcontents
\end{adjustwidth}
\end{center}
}
%%
%% Section Headings 
%%

\makeatother
\renewcommand{\cfttoctitlefont}{\Large\bfseries}
\renewcommand{\contentsname}{Inhaltsverzeichnis}
\renewcommand{\cftsecfont}{\normalfont}
\renewcommand{\cftsecpresnum}{\S~}
\renewcommand{\cftsecaftersnum}{.~}
\setlength{\cftsecnumwidth}{0.9cm}
\renewcommand{\cftsecdotsep}{3.5}
\renewcommand{\cftsecpagefont}{\itshape}
\renewcommand{\cftsecleader}{~~~\cftdotfill{\cftsecdotsep}}
\renewcommand{\cftdot}{.}
\cftsetpnumwidth{0.7cm}
\setlength{\cftbeforesecskip}{0.1cm}

\renewcommand{\thesection}{\arabic{section}}
\makeatletter
\def\@seccntformat#1{{\bfseries\S}~\csname the#1\endcsname.\quad}
\makeatother
\sectionfont{\LARGE\rmfamily\bfseries\itshape}
\renewcommand{\sec}[1]{%
\vspace*{-1.5cm}
\section{#1}
\renewcommand{\leftmark}[1]{{\em \S~\arabic{section}. ~~ #1}}
\thispagestyle{plain}
\setcounter{equation}{0}
\setcounter{theno}{0}
\vspace*{0.0cm}
}

%%
%% Theorem Environments 
%%

\newcounter{theno}
\newcounter{labno}
\newcounter{aufgno}
\makeatletter
\@addtoreset{theno}{section}
\makeatother
%% Proof

\newenvironment{proof}[1][\empty]{%
\begin{adjustwidth}{1.5em}{0em}
{\itshape Beweis:}
{\ifx\empty#1\else\relax (#1)\fi} ~
}{%
\qed\end{adjustwidth}
}

\newcommand{\qed}{\hfill$\square$\\}


\newenvironment{loesung}[1][\empty]{%
\begin{adjustwidth}{1.5em}{0em}
{\itshape Lösung:}
{\ifx\empty#1\else\relax (#1)\fi} ~
}{%
\qed\end{adjustwidth}
}

%% Definition 

\newenvironment{definition}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Def. \arabic{section}.\arabic{theno})}\else#1\fi}
\refstepcounter{labno}\definecolor{shadecolor}{rgb}{0.90,0.90,0.90}
\begin{center}
\begin{minipage}{14cm}
\begin{shaded}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Definition \arabic{section}.\arabic{theno}}
{\ifx\empty#1\relax\else(#1)\newline\fi}~~
}{%
\end{shaded} 
\end{minipage}
\end{center}}

%% Proposition

\newenvironment{proposition}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Prop.\ \arabic{section}.\arabic{theno})}\else#1\fi}
\refstepcounter{labno}
\begin{center}
\begin{minipage}{14cm}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Proposition \arabic{section}.\arabic{theno}}
{\ifx\empty#1\relax\else(#1)\newline\fi}~~ 
}{%
\end{minipage}
\end{center}}

%% Theorem

\newenvironment{theorem}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Satz \arabic{section}.\arabic{theno}}\else#1\fi}
\refstepcounter{labno}\definecolor{shadecolor}{rgb}{0.90,0.90,0.90}
\begin{center}
\begin{minipage}{14cm}
\begin{shaded}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Satz \arabic{section}.\arabic{theno}}
{\ifx\empty#1\relax\else(#1)\newline\fi}~~
}{%
\end{shaded} 
\end{minipage}
\end{center}}

%% Folgerung

\newenvironment{corollary}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Folgerung \arabic{section}.\arabic{theno}}\else#1\fi}
\refstepcounter{labno}\definecolor{shadecolor}{rgb}{0.90,0.90,0.90}
\begin{center}
\begin{minipage}{14cm}
\begin{shaded}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Folgerung \arabic{section}.\arabic{theno}}
{\ifx\empty#1\relax\else(#1)\newline\fi}~~
}{%
\end{shaded} 
\end{minipage}
\end{center}}

%% Lemma

\newenvironment{lemma}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Lemma \arabic{section}.\arabic{theno}}\else#1\fi}
\refstepcounter{labno}\definecolor{shadecolor}{rgb}{1.00,1.00,1.00}
\begin{center}
\begin{minipage}{14cm}
\begin{shaded}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Lemma \arabic{section}.\arabic{theno}} 
{\ifx\empty#1\relax\else(#1)\newline\fi}~~ 
}{%
\end{shaded} 
\end{minipage}
\end{center}}

%%Erinnerung

\newenvironment{erinnerung}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Erinn. \arabic{section}.\arabic{theno})}\else#1\fi}
\refstepcounter{labno}
\begin{center}
\begin{minipage}{14cm}
\begin{framed}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Erinnerung \arabic{section}.\arabic{theno}}
{\ifx\empty#1\relax\else(#1)\fi}~~ 
}{%
\end{framed}
\end{minipage}
\end{center}}

%%Bemerkung

\newenvironment{bemerkung}[1][\empty]{%
\stepcounter{theno}
\renewcommand{\thelabno}{\ifx\empty#1{Bem. \arabic{section}.\arabic{theno})}\else#1\fi}
\refstepcounter{labno}
\begin{center}
\begin{minipage}{14cm}
\label{satz\arabic{section}.\arabic{theno}}
{\bfseries Bemerkung \arabic{section}.\arabic{theno}}
{\ifx\empty#1\relax\else(#1)\fi}~~ 
}{%
\end{minipage}
\end{center}
}


%% Aufgabe

\newenvironmentx{aufgabe}[2][1=\empty]{%
\stepcounter{aufgno}
\renewcommand{\thelabno}{Aufgabe~#2 \ifx\empty#1\relax\else(#1)\fi}
\refstepcounter{labno}
\definecolor{shadecolor}{rgb}{0.90,0.90,0.90}
\begin{center}
\begin{minipage}{14cm}
\begin{framed}
\label{aufg:#2}
{\bfseries Aufgabe #2 \ifx\empty#1\relax\else(#1)\fi}\newline 
}{%
\end{framed}
\end{minipage}
\end{center}}

%% Teilaufgabe

\newenvironment{teilaufgabe}[2][\empty]{%
\renewcommand{\thelabno}{\ifx\empty#1 Aufgabe~\arabic{aufgno}\else#1~(\alphalph{#2})\fi}
\refstepcounter{labno}
\definecolor{shadecolor}{rgb}{0.90,0.90,0.90}
\begin{center}
\begin{minipage}{14cm}
\begin{framed}
\begin{enumerate}[(a), start=#2]
}{%
\end{enumerate}
\end{framed}
\end{minipage}
\end{center}}

%%Not Numbered

\newenvironment{notnumbered}[1]{
\definecolor{shadecolor}{rgb}{1.00,1.00,1.00}
\begin{center}
\begin{minipage}{14cm}
\begin{shaded}
{\bfseries #1} ~~ 
}{%
\end{shaded} 
\end{minipage}
\end{center}}

%%Beispiel

\newcounter{bspno}
\newenvironment{bsp}[1][\empty]{%
\stepcounter{bspno}
\renewcommand{\thelabno}{Beispiele}
\refstepcounter{labno}
\begin{adjustwidth}{1.5em}{0em}
{\bfseries Beispiele } 
{\ifx\empty#1\relax\else(#1)\fi}~
}{%
\end{adjustwidth}
}



%%
%% Enumeration
%%

\newcounter{fig}
\newenvironment{enum}{%
\begin{list}{(\roman{fig})}%
{\usecounter{fig}
\setlength{\leftmargin}{1.0cm}
\setlength{\topsep}{0.1cm}
\setlength{\itemsep}{0.0cm}
\setlength{\parsep}{0.0cm}
\labelwidth1cm}}
{\end{list}}



\newenvironment{enumliteral}{%
\begin{list}{(\alph{fig})}%
{\usecounter{fig}
\setlength{\leftmargin}{1.0cm}
\setlength{\topsep}{0.1cm}
\setlength{\itemsep}{0.0cm}
\setlength{\parsep}{0.0cm}
\labelwidth1cm}}
{\end{list}}

\newenvironment{wideenum}{%%\includeonly{}

\begin{list}{(\roman{fig})}
{\usecounter{fig}
\setlength{\labelwidth}{1.5cm}
\setlength{\labelsep}{0.2cm}
\setlength{\leftmargin}{1.0cm}
\setlength{\topsep}{0.3cm}
\setlength{\itemsep}{0.2cm}
\setlength{\parsep}{0.0cm}}}%
{\end{list}}

\newenvironment{rapid}{%
\begin{list}{\----~~~~}
{\usecounter{fig}
\setlength{\labelwidth}{1.5cm}
\setlength{\labelsep}{0.2cm}
\setlength{\leftmargin}{1.0cm}
\setlength{\topsep}{0.3cm}
\setlength{\itemsep}{0.2cm}
\setlength{\parsep}{0.0cm}}}%
{\end{list}\rmfamily}

%% Hervorhebungen

\renewcommand{\em}[1]{{\bfseries\slshape #1}}


%%
%% Beginning of Document
%% 

\AtBeginDocument{%
\maketitle
\pagestyle{fancy}
\fontsize{11}{15}\selectfont
\setlength{\parsep}{0.3cm}
\setlength{\parskip}{0.5cm}
\hfuzz=2pt
\vbadness=10000
\raggedbottom}

